/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.incurrency.framework;

/**
 *
 * @author psharma
 */
import java.util.concurrent.ConcurrentHashMap;

public class CaseInsensitiveConcurrentHashMap <T> extends ConcurrentHashMap<String, T> {
   
@Override
    public T put(String key, T value) {
        return super.put(key.toLowerCase(), value);
    }

    public T get(String key) {
        return super.get(key.toLowerCase());
    }

}
