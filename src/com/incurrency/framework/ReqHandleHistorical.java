/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.incurrency.framework;

import java.util.Date;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class ReqHandleHistorical implements Runnable {

    private static final Logger logger = Logger.getLogger(ReqHandleHistorical.class.getName());

    Thread t;
    public int reqCounter = 1;
    public Date lastRequestTime = new Date();
    public int delay = 10;//in seconds
    private boolean terminate = false;

    public ReqHandleHistorical(String name) {
 //       t = new Thread(this, "Historical Data Handler: " + name);
//         t.setName("Historical Data Handle Provider");
//        t.start();
    }

    public synchronized boolean getHandle(BeanConnection c,int outstandingRequestsThreshold,int delay) {
        reqCounter++;
        try {
            if (new Date().getTime() < lastRequestTime.getTime() + delay * 1000) {
                long sleepTime = lastRequestTime.getTime() + delay * 1000 - new Date().getTime();
                if (sleepTime > 0) {
                    Thread.sleep(sleepTime);
                }
                reqCounter = 1;
            }
            int wait=0;
            if(c.getOutstandingHistoricalRequests()>=outstandingRequestsThreshold ){
                while(c.getOutstandingHistoricalRequests()!=0 & c.getOutstandingHistoricalRequests()>=outstandingRequestsThreshold & wait<1000){
                logger.log(Level.FINE,"Request Handle Sleeping for 5 seconds for {0} time",new  Object[]{wait});
                TreeMap<Integer,Request> pending=c.getWrapper().getOpenRequests(EnumRequestType.HISTORICAL);
                for(Integer reqid:pending.keySet()){
                    Request r=c.getWrapper().getRequest(reqid);
                    if(r.requestStatus.equals(EnumRequestStatus.PENDING) && new Date().getTime()-r.requestTime > 10*60*1000){
                    c.getWrapper().cancelHistoricalData(r.requestID);
                    logger.log(Level.INFO,"Cancellation Request Sent for ReqID: {0}",new Object[]{r.requestID});
                    //c.getWrapper().getRequest(reqid).requestStatus=EnumRequestStatus.CANCELLED;
                    //c.decrementOutstandingHistoricalRequests(); 
//                    String barsize=null;
//                    switch(r.barSize){
//                        case ONESECOND:
//                            barsize="1 secs";
//                            break;
//                        case ONEMINUTE:
//                            barsize="1 min";
//                            break;
//                        case DAILY:
//                            barsize="1 day";
//                            break;
//                        default:
//                            break;
//                    }
//                    c.getWrapper().requestHistoricalData(r.symbol, r.endTime, r.duration, barsize);                        
//                    Thread.sleep(100);
//                    Thread.yield();
                    }
                }
                Thread.sleep(5000);  
                Thread.yield();
                wait=wait+1;
                }
            }
            if(wait>=1000){
                c.decrementOutstandingHistoricalRequests(); 
            }
            lastRequestTime = new Date();
            return true;
        } catch (Exception e) {
            logger.log(Level.INFO, "101", e);
            return false;
        }
    }

    @Override
    public synchronized void run() {
        //while (true && !terminate){

        //}
        while (!Thread.currentThread().isInterrupted()) {
            try {
                wait();
            } catch (InterruptedException ex) {
                logger.log(Level.INFO, "101", ex);
            }
        }
    }

}
